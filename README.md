# Wishlist UWP applicatie

Gemaakt door:

* [Nicolas Loots](https://gitlab.com/GXGOW)
* [Fien Spriet](https://gitlab.com/fienspriet)
* [Dieter Mailly](https://gitlab.com/dierke9)

Groep: G4

## Hoe opzetten

Om de database in te stellen, run je eerst volgende commando's (zorg ervoor dat WishlistBackend ingesteld is als Startup project):

```PowerShell
Add-Migration InitialMigration
Update-Database
```

Daarna kan je beginnen met een nieuwe account aan te maken. Veel plezier!