﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WishlistBackend.ViewModels
{
    public class UserSettingPasswordViewModel
    {
        public String NewPassword { get; set; }
        public String RepeatPassword { get; set; }
    }
}
