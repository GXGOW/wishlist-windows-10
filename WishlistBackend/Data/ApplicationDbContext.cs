﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WishlistBackend.Models;

namespace WishlistBackend.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<WishList> WishList { get; set; }

        public DbSet<User> User { get; set; }

        public DbSet<JWTToken> JWTTokens { get; set; }

        public DbSet<WishListItem> WishListItem { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<JoinTable_Friends>(Map_JoinTable_Friends);
            modelBuilder.Entity<JoinTable_Invites>(Map_JoinTable_Invites);
            modelBuilder.Entity<JoinTable_Requests>(Map_JoinTable_Requests);
            modelBuilder.Entity<Category>().HasData(
                new Category { CategoryId = 1, Name = "Clothing", Default = true },
                new Category { CategoryId = 2, Name = "Multimedia", Default = true },
                new Category { CategoryId = 3, Name = "Toys", Default = true },
                new Category { CategoryId = 4, Name = "Food and drinks", Default = true },
                new Category { CategoryId = 5, Name = "Furniture", Default = true },
                new Category { CategoryId = 6, Name = "Sport", Default = true },
                new Category { CategoryId = 7, Name = "Accessories", Default = true },
                new Category { CategoryId = 8, Name = "Traveling", Default = true });
        }

        private static void Map_JoinTable_Friends(EntityTypeBuilder<JoinTable_Friends> uw)
        {
            uw.HasKey(t => new { t.UserId, t.WishlistId });
            uw.HasOne(t => t.User)
                .WithMany(u => u.WishlistsFriends);
            uw.HasOne(t => t.Wishlist)
                .WithMany(u => u.Friends);
        }

        private static void Map_JoinTable_Invites(EntityTypeBuilder<JoinTable_Invites> uw)
        {
            uw.HasKey(t => new { t.UserId, t.WishlistId });
            uw.HasOne(t => t.User)
                .WithMany(u => u.Invites);
            uw.HasOne(t => t.Wishlist)
                .WithMany(u => u.Invites);
        }

        private static void Map_JoinTable_Requests(EntityTypeBuilder<JoinTable_Requests> uw)
        {
            uw.HasKey(t => new { t.UserId, t.WishlistId });
            uw.HasOne(t => t.User)
                .WithMany(u => u.Requests);
            uw.HasOne(t => t.Wishlist)
                .WithMany(u => u.Requests);
        }

        public DbSet<Category> Category { get; set; }
    }
}
