﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace WishlistBackend.Models
{
    public class JWTToken
    {
        public int JWTTokenId { get; set; }
        public string Value { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }

        public JWTToken(int userId)
        {
            UserId = userId;
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Value = new string(Enumerable.Repeat(chars, 25)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
