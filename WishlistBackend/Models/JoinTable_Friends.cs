﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WishlistBackend.Models
{
    public class JoinTable_Friends
    {
        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        [ForeignKey("Whitelist")]
        public int WishlistId { get; set; }
        public virtual WishList Wishlist { get; set; }
    }
}
