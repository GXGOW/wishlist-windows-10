﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WishlistBackend.Models
{
    public class WishListItem
    {
        public int WishListItemId { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public bool Bought { get; set; }
        public User BoughtBy { get; set; }
        public byte[] image { get; set; }
        public virtual Category Category { get; set; }
        [ForeignKey("Category")]
        public int CategoryId;
    }
}
