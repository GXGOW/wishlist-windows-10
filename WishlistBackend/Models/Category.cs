﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WishlistBackend.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        public String Name { get; set; }
        [ForeignKey("User")]
        public int? UserId { get; set; }
        public bool Default { get; set; }
    }
}
