﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WishlistBackend.Models
{
    public class JoinTable_Requests
    {
        [ForeignKey("User")]
        public int UserId { get; set; }
        public User User { get; set; }
        [ForeignKey("Wishlist")]
        public int WishlistId { get; set; }
        public WishList Wishlist { get; set; }
    }
}
