﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WishlistBackend.Models
{
    public class WishList
    {
        public int WishListId { get; set; }
        [JsonIgnore]
        public virtual List<JoinTable_Friends> Friends { get; set; }
        [JsonIgnore]
        public virtual List<JoinTable_Invites> Invites { get; set; }
        [JsonIgnore]
        public virtual List<JoinTable_Requests> Requests { get; set; }
        public virtual List<WishListItem> Items { get; set; }
        public String Name { get; set; }
        public DateTime Deadline { get; set; }

        public WishList()
        {
            this.Items = new List<WishListItem>();
            this.Friends = new List<JoinTable_Friends>();
        }
    }
}
