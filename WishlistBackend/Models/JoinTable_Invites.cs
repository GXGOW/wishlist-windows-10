﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WishlistBackend.Models
{
    public class JoinTable_Invites
    {
        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        [ForeignKey("Wishlist")]
        public int WishlistId { get; set; }
        public WishList Wishlist { get; set; }
    }
}
