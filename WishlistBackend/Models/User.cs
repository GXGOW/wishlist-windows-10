﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WishlistBackend.Models
{
    public class User
    {
        public int UserId { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        [JsonIgnore]
        public virtual List<WishList> OwnWishlists { get; set; }
        [JsonIgnore]
        public virtual List<JoinTable_Friends> WishlistsFriends { get; set; }
        [JsonIgnore]
        public virtual List<JoinTable_Invites> Invites { get; set; }
        [JsonIgnore]
        public virtual List<JoinTable_Requests> Requests { get; set; }
        public byte[] ProfilePicture { get; set; }


        public User()
        {
            this.OwnWishlists = new List<WishList>();
        }

    }
}
