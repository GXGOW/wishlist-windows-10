using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WishlistBackend.Models;
using WishlistBackend.ViewModels;

namespace WishlistBackend.Controllers
{
    [Produces("application/json")]
    [Route("api/Users")]
    public class UsersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public UsersController(ApplicationDbContext context)
        {
            _context = context;
        }

        private IQueryable<User> AllUsersWithIncludes()
        {
            return _context.User
                .Include(u => u.OwnWishlists)
                .ThenInclude(own=>own.Requests)
                .Include(u => u.WishlistsFriends)
                .Include(u => u.Invites)
                .Include(u => u.Requests);
        }

        // GET: api/Users
        [HttpGet]
        public IEnumerable<User> GetUser()
        {
            return AllUsersWithIncludes();
        }

        [Route("~/api/login")]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] Tuple<string, string> credentials)
        {
            User validUser = AllUsersWithIncludes()
                .FirstOrDefault(u => u.Username == credentials.Item1 && u.Password == credentials.Item2);
            if (validUser != null)
            {
                var jwt = await GenerateJWTToken(validUser.UserId);
                return Ok((validUser, jwt));
            }
            else
            {
                return NotFound();
            }
        }

        private async Task<string> GenerateJWTToken(int userId)
        {
            JWTToken token = new JWTToken(userId);
            _context.JWTTokens.Add(token);
            await _context.SaveChangesAsync();
            return token.Value;
        }

        [Route("~/api/register")]
        [HttpPost]
        public async Task<IActionResult> Register([FromBody] User user)
        {
            if (user.Username == null || user.Username.Equals("") || user.Password == null || user.Password.Equals(""))
            {
                return NotFound();
            }
            _context.User.Add(user);
            await _context.SaveChangesAsync();
            return Ok(user);
        }

        [Route("~/api/logout")]
        [HttpPost]
        public async Task<IActionResult> Logout([FromBody] string token)
        {
            _context.JWTTokens.Remove(_context.JWTTokens.FirstOrDefault(t => t.Value == token));
            await _context.SaveChangesAsync();
            return Ok();
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await AllUsersWithIncludes().SingleOrDefaultAsync(m => m.UserId == id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser([FromRoute] int id, [FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.UserId)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Users
        [HttpPost]
        public async Task<IActionResult> PostUser([FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.User.Add(user);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUser", new { id = user.UserId }, user);
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _context.User.SingleOrDefaultAsync(m => m.UserId == id);
            if (user == null)
            {
                return NotFound();
            }

            _context.User.Remove(user);
            await _context.SaveChangesAsync();

            return Ok(user);
        }

        private bool UserExists(int id)
        {
            return _context.User.Any(e => e.UserId == id);
        }

        // GET: api/Users/5/OwnLists
        [HttpGet("{id}/OwnLists")]
        public async Task<IActionResult> GetWhishListFromUser([FromRoute] int id)
        {
            User user = await AllUsersWithIncludes().SingleOrDefaultAsync(m => m.UserId == id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user.OwnWishlists);
        }

        // GET: api/Users/FriendsOfWishlist/5
        [HttpGet("FriendsOfWishlist/{wishlistid}")]
        public IActionResult FriendsOfWishlist([FromRoute] int wishlistid)
        {
            var users = AllUsersWithIncludes();
            List<User> friendsOfWishlist = new List<User>();

            foreach (User user in users)
            {
                foreach (JoinTable_Friends friend in user.WishlistsFriends)
                {
                    if (friend.WishlistId == wishlistid) friendsOfWishlist.Add(user);
                }
            }
            return Ok(friendsOfWishlist);
        }

        // GET: api/Users/InvitedUsersOfWishlist/5
        [HttpGet("InvitedUsersOfWishlist/{wishlistid}")]
        public IActionResult InvitedUsersOfWishlist([FromRoute] int wishlistid)
        {
            var users = AllUsersWithIncludes();
            List<User> invitesOfWishlist = new List<User>();

            foreach (User user in users)
            {
                foreach (JoinTable_Invites invite in user.Invites)
                {
                    if (invite.WishlistId == wishlistid) invitesOfWishlist.Add(user);
                }
            }
            return Ok(invitesOfWishlist);
        }

        // GET: api/Users/RequestedUsersOfWishlist/5
        [HttpGet("RequestedUsersOfWishlist/{wishlistid}")]
        public IActionResult RequestedUsersOfWishlist([FromRoute] int wishlistid)
        {
            var users = AllUsersWithIncludes();
            List<User> requestsOfWishlist = new List<User>();

            foreach (User user in users)
            {
                foreach (JoinTable_Requests request in user.Requests)
                {
                    if (request.WishlistId == wishlistid) requestsOfWishlist.Add(user);
                }
            }
            return Ok(requestsOfWishlist);
        }

        // PUT: api/Users/UpdateEmail/5
        [HttpPut("UpdateEmail/{userId}")]
        public async Task<IActionResult> UpdateEmail([FromBody] UserSettingEmailViewModel newEmail, [FromRoute] int userId)
        {
            User user = _context.User.SingleOrDefault(u => u.UserId == userId);
            if (user == null)
            {
                return BadRequest("User not found");
            }
            if(newEmail.NewEmail == null || newEmail.NewEmail == "")
            {
                return BadRequest("Email cannot be empty");
            }
            if(newEmail.NewEmail != newEmail.RepeatEmail)
            {
                return BadRequest("Email and repeat email are not the same");
            }
            user.Username = newEmail.NewEmail;
            _context.Attach(user);
            _context.Entry(user).Property("Username").IsModified = true;
            await _context.SaveChangesAsync();
            return Ok(user);
        }

        // PUT: api/Users/UpdatePassword/5
        [HttpPut("UpdatePassword/{userId}")]
        public async Task<IActionResult> UpdatePassword([FromBody] UserSettingPasswordViewModel newPwd, [FromRoute] int userId)
        {
            User user = _context.User.SingleOrDefault(u => u.UserId == userId);
            if (user == null)
            {
                return BadRequest("User not found");
            }
            if (newPwd.NewPassword == null || newPwd.NewPassword == "")
            {
                return BadRequest("Password cannot be empty");
            }
            if (newPwd.NewPassword != newPwd.RepeatPassword)
            {
                return BadRequest("Password and repeat password are not the same");
            }
            user.Password = newPwd.NewPassword;
            _context.Attach(user);
            _context.Entry(user).Property("Password").IsModified = true;
            await _context.SaveChangesAsync();
            return Ok(user);
        }

        [HttpPut("UpdateProfilePicture/{userId}")]
        public async Task<ActionResult> UpdateProfilePicture([FromRoute] int userId)
        {
            User user = _context.User.SingleOrDefault(u => u.UserId == userId);
            if (user == null)
            {
                return BadRequest("User not found");
            }
            using (var ms = new MemoryStream(2048))
            {
                await Request.Body.CopyToAsync(ms);
                byte[] picture =  ms.ToArray();
                user.ProfilePicture = picture;
                _context.Attach(user);
                _context.Entry(user).Property("ProfilePicture").IsModified = true;
                await _context.SaveChangesAsync();
                return Ok(user);
            }
        }

        //POST: api/Users/jwt
        [HttpPost("jwt")]
        public IActionResult ValidateToken([FromBody] string token)
        {
            JWTToken jwt = _context.JWTTokens.FirstOrDefault(t => t.Value == token);
            if (jwt != null)
            {
                User user = AllUsersWithIncludes().FirstOrDefault(u => u.UserId == jwt.UserId);
                if(user != null)
                {
                    return Ok(user);
                }
                return NotFound();
            }
            return NotFound();
        }

        private void RemoveTokensForUser(int userId)
        {
            _context.JWTTokens.RemoveRange(_context.JWTTokens.Where(t => t.UserId == userId));
            _context.SaveChanges();
        }

        [HttpPost("AcceptInviteToWishlist/{userid}")]
        public async Task<IActionResult> AcceptInviteToWishlist([FromBody] int wishlistid, [FromRoute] int userid)
        {
            User user = await AllUsersWithIncludes().SingleOrDefaultAsync(m => m.UserId == userid);
            if (user == null)
            {
                return NotFound();
            }

            JoinTable_Invites invite = user.Invites.Find(i => i.WishlistId == wishlistid);
            if (invite == null)
            {
                return NotFound();
            }
            JoinTable_Friends friendswishlist = new JoinTable_Friends() { UserId = userid, WishlistId = wishlistid };

            //Alter invitation to wishlist of a friend.
            user.Invites.Remove(invite);

            //Also remove an open request for this wishlist by the user if it exists.
            JoinTable_Requests request = user.Requests.Find(r => r.WishlistId == wishlistid && r.UserId == userid);
            if (request != null)
            {
                user.Requests.Remove(request);
            }

            user.WishlistsFriends.Add(friendswishlist);

            await _context.SaveChangesAsync();

            return Ok(user.WishlistsFriends);
        }

        [HttpPost("RejectInviteToWishlist/{userid}")]
        public async Task<IActionResult> RejectInviteToWishlist([FromBody] int wishlistid, [FromRoute] int userid)
        {
            User user = await AllUsersWithIncludes().SingleOrDefaultAsync(m => m.UserId == userid);
            if (user == null)
            {
                return NotFound();
            }

            JoinTable_Invites invite = user.Invites.Find(i => i.WishlistId == wishlistid);
            if (invite == null)
            {
                return NotFound();
            }
            //Remove invitation to wishlist of a friend.
            user.Invites.Remove(invite);

            await _context.SaveChangesAsync();

            return Ok(user.WishlistsFriends);
        }

        [HttpPost("RejectAllRequestsFromUser/{userid}")]
        public async Task<IActionResult> RejectAllRequestsFromUser([FromBody] int requested_userid, [FromRoute] int userid)
        {
            User user = await AllUsersWithIncludes().SingleOrDefaultAsync(m => m.UserId == userid);
            User requested_user = await AllUsersWithIncludes().SingleOrDefaultAsync(m => m.UserId == requested_userid);
            if (user == null || requested_user == null)
            {
                return NotFound();
            }

            List<JoinTable_Requests> requests = new List<JoinTable_Requests>();

            foreach (WishList userWishlist in user.OwnWishlists)
            {
                JoinTable_Requests request = userWishlist.Requests.Find(r => r.UserId == requested_userid);
                if (request != null)
                {
                    requests.Add(request);
                }
            }

            if (requests.Count == 0)
            {
                return NotFound();
            }

            foreach (JoinTable_Requests request in requests)
            {
                //Alter request to wishlist of a friend.
                requested_user.Requests.Remove(request);
            }

            await _context.SaveChangesAsync();

            return Ok(user.WishlistsFriends);
        }

        [HttpPost("AcceptAllRequestsFromUser/{userid}")]
        public async Task<IActionResult> AcceptAllRequestsFromUser([FromBody] int requested_userid, [FromRoute] int userid)
        {
            User user = await AllUsersWithIncludes().SingleOrDefaultAsync(m => m.UserId == userid);
            User requested_user = await AllUsersWithIncludes().SingleOrDefaultAsync(m => m.UserId == requested_userid);
            if (user == null || requested_user == null)
            {
                return NotFound();
            }

            List<JoinTable_Requests> requests = new List<JoinTable_Requests>();

            foreach (WishList userWishlists in user.OwnWishlists)
            {
                JoinTable_Requests request = userWishlists.Requests.Find(r => r.UserId == requested_userid);
                if (request != null)
                {
                    requests.Add(request);
                }
            }

            if (requests.Count == 0)
            {
                return NotFound();
            }

            foreach (JoinTable_Requests request in requests)
            {
                JoinTable_Friends friendswishlist = new JoinTable_Friends() { UserId = requested_userid, WishlistId = request.WishlistId };

                //Alter request to wishlist of a friend.
                requested_user.Requests.Remove(request);

                //Also remove an open invite for this wishlist by the user if it exists.
                JoinTable_Invites invite = user.Invites.Find(r => r.WishlistId == request.WishlistId && r.UserId == requested_userid);
                if (invite != null)
                {
                    requested_user.Invites.Remove(invite);
                }

                requested_user.WishlistsFriends.Add(friendswishlist);
            }

            await _context.SaveChangesAsync();

            return Ok(user.WishlistsFriends);
        }

        // GET: api/Users/OwnerOfWishlist/5
        [HttpGet("OwnerOfWishlist/{wishlistid}")]
        public IActionResult OwnerOfWishlist([FromRoute] int wishlistid)
        {
            var users = AllUsersWithIncludes();
            User owner = null;

            foreach (User user in users)
            {
                foreach (WishList wishlist in user.OwnWishlists)
                {
                    if (wishlist.WishListId == wishlistid) owner = user;
                }
            }

            if (owner == null)
            {
                return NotFound();
            }
            return Ok(owner);
        }

        [HttpPost("CreateRequestsToAllWishlistsOfUser/{friendid}")]
        public async Task<IActionResult> CreateRequest([FromBody] User user, [FromRoute] int friendid)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            User friend = await AllUsersWithIncludes().SingleOrDefaultAsync(m => m.UserId == friendid);
            if (friend == null)
            {
                return NotFound();
            }

            foreach (WishList wishlist in friend.OwnWishlists)
            {
                wishlist.Requests.Add(new JoinTable_Requests() { UserId = user.UserId, WishlistId = wishlist.WishListId });
            }

            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}