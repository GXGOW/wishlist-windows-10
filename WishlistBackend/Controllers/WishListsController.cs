using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WishlistBackend.Models;

namespace WishlistBackend.Controllers
{
    [Produces("application/json")]
    [Route("api/WishLists")]
    public class WishListsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public WishListsController(ApplicationDbContext context)
        {
            _context = context;
        }

        private IQueryable<WishList> AllWishlistsWithIncludes()
        {
            return _context.WishList
                .Include(t => t.Friends)
                .Include(t => t.Invites)
                .Include(t => t.Requests)
                .Include(t => t.Items).ThenInclude(i => i.Category)
                .Include(t => t.Items).ThenInclude(i => i.BoughtBy);
        }

        // GET: api/WishLists
        [HttpGet]
        public IEnumerable<WishList> GetWishList()
        {
            return _context.WishList;
        }

        // GET: api/WishLists/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetWishList([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var wishList = await AllWishlistsWithIncludes()
                .SingleOrDefaultAsync(m => m.WishListId == id);

            if (wishList == null)
            {
                return NotFound();
            }

            return Ok(wishList);
        }

        // PUT: api/WishLists/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWishList([FromRoute] int id, [FromBody] WishList wishList)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != wishList.WishListId)
            {
                return BadRequest();
            }

            _context.Entry(wishList).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WishListExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/WishLists
        [HttpPost]
        public async Task<IActionResult> PostWishList([FromBody] WishList wishList)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.WishList.Add(wishList);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetWishList", new { id = wishList.WishListId }, wishList);
        }

        // DELETE: api/WishLists/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWishList([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var wishList = await _context.WishList.SingleOrDefaultAsync(m => m.WishListId == id);
            if (wishList == null)
            {
                return NotFound();
            }

            _context.WishList.Remove(wishList);
            await _context.SaveChangesAsync();

            return Ok(wishList);
        }

        // GET: api/WishLists/InvitedWishlistsOfUser/5
        [HttpGet("InvitedWishlistsOfUser/{userid}")]
        public IActionResult GetInvitedWishlistsOfUser([FromRoute] int userid)
        {
            var wishlists = AllWishlistsWithIncludes();
            List<WishList> invitedWishlists = new List<WishList>();

            foreach (WishList list in wishlists)
            {
                foreach (JoinTable_Invites invite in list.Invites)
                {
                    if (invite.UserId == userid) invitedWishlists.Add(list);
                }
            }
            return Ok(invitedWishlists);
        }

        // GET: api/WishLists/OthersWishlistsOfUser/5
        [HttpGet("OthersWishlistsOfUser/{userid}")]
        public IActionResult GetSharedWishlistsOfUser([FromRoute] int userid)
        {
            var wishlists = AllWishlistsWithIncludes();
            List<WishList> friendsWishlists = new List<WishList>();

            foreach (WishList list in wishlists)
            {
                foreach (JoinTable_Friends friend in list.Friends)
                {
                    if (friend.UserId == userid) friendsWishlists.Add(list);
                }
            }
            return Ok(friendsWishlists);
        }

        // GET: api/WishLists/RequestedWishlistsOfUser/5
        [HttpGet("RequestedWishlistsOfUser/{userid}")]
        public IActionResult GetRequistedWishlistsOfUser([FromRoute] int userid)
        {
            var wishlists = AllWishlistsWithIncludes();
            List<WishList> requistedWishlists = new List<WishList>();

            foreach (WishList list in wishlists)
            {
                foreach (JoinTable_Requests request in list.Requests)
                {
                    if (request.UserId == userid) requistedWishlists.Add(list);
                }
            }
            return Ok(requistedWishlists);
        }

        //Create a new own wishlist for user
        [HttpPost("addToUser/{id}")]
        public async Task<IActionResult> AddWishlistToUser([FromBody] WishList list, [FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var user = _context.User.SingleOrDefault(m => m.UserId == id);
            if (user == null)
            {
                return NotFound();
            }

            _context.WishList.Add(list);
            user.OwnWishlists.Add(list);
            await _context.SaveChangesAsync();
            return Ok(list);
        }

        private bool WishListExists(int id)
        {
            return _context.WishList.Any(e => e.WishListId == id);

        }

        [HttpPost("inviteFriend/{wishlistid}")]
        public async Task<IActionResult> inviteFriendToWishlist([FromBody] int invited_userid, [FromRoute] int wishlistid)
        {
            WishList wishList = await AllWishlistsWithIncludes().SingleOrDefaultAsync(m => m.WishListId == wishlistid);
            User user = _context.User.SingleOrDefault(m => m.UserId == invited_userid);

            if (wishList == null || user == null)
            {
                return NotFound();
            }

            JoinTable_Invites invite = new JoinTable_Invites() { User = user, UserId = invited_userid, Wishlist = wishList, WishlistId = wishlistid };

            wishList.Invites.Add(invite);
            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("AcceptRequestToWishlist/{wishlistid}")]
        public async Task<IActionResult> AcceptRequestToWishlist([FromBody] int requested_userid, [FromRoute] int wishlistid)
        {
            WishList wishList = await AllWishlistsWithIncludes().SingleOrDefaultAsync(m => m.WishListId == wishlistid);
            User requested_user = _context.User.SingleOrDefault(m => m.UserId == requested_userid);
            if (wishList == null || requested_user == null)
            {
                return NotFound();
            }

            JoinTable_Requests request = wishList.Requests.Find(r => r.UserId == requested_userid);

            if (request == null)
            {
                return NotFound();
            }

            JoinTable_Friends friendswishlist = new JoinTable_Friends() { UserId = requested_userid, WishlistId = wishlistid };

            //Alter invitation to wishlist of a friend.
            wishList.Requests.Remove(request);

            //Also remove an open invite for this wishlist by the user if it exists.
            JoinTable_Invites invite = wishList.Invites.Find(r => r.WishlistId == request.WishlistId && r.UserId == requested_userid);
            if (invite != null)
            {
                wishList.Invites.Remove(invite);
            }

            wishList.Friends.Add(friendswishlist);

            await _context.SaveChangesAsync();

            return Ok(wishList.Friends);
        }

        [HttpPost("RejectRequestToWishlist/{wishlistid}")]
        public async Task<IActionResult> RejectRequestToWishlist([FromBody] int requested_userid, [FromRoute] int wishlistid)
        {
            WishList wishList = await AllWishlistsWithIncludes().SingleOrDefaultAsync(m => m.WishListId == wishlistid);
            User requested_user = _context.User.SingleOrDefault(m => m.UserId == requested_userid);
            if (wishList == null || requested_user == null)
            {
                return NotFound();
            }

            JoinTable_Requests request = wishList.Requests.Find(r => r.UserId == requested_userid);

            if (request == null)
            {
                return NotFound();
            }

            //Alter invitation to wishlist of a friend.
            wishList.Requests.Remove(request);

            await _context.SaveChangesAsync();

            return Ok(wishList.Friends);
        }

        [HttpGet("AcceptAllRequests/{wishlistid}")]
        public async Task<IActionResult> AcceptAllRequests([FromRoute] int wishlistid)
        {
            WishList wishList = await AllWishlistsWithIncludes().SingleOrDefaultAsync(m => m.WishListId == wishlistid);
            if (wishList == null)
            {
                return NotFound();
            }
            
            foreach (JoinTable_Requests request in wishList.Requests)
            {
                JoinTable_Friends friendswishlist = new JoinTable_Friends() { UserId = request.UserId, WishlistId = wishlistid};

                //Alter invitation to wishlist of a friend.
                wishList.Friends.Add(friendswishlist);

                //Also remove an open invite for this wishlist by the user if it exists.
                JoinTable_Invites invite = wishList.Invites.Find(r => r.WishlistId == request.WishlistId && r.UserId == request.UserId);
                if (invite != null)
                {
                    wishList.Invites.Remove(invite);
                }
            }

            wishList.Requests = new List<JoinTable_Requests>();

            await _context.SaveChangesAsync();

            return Ok(wishList.Friends);
        }

        [HttpGet("RejectAllRequests/{wishlistid}")]
        public async Task<IActionResult> RejectAllRequests([FromRoute] int wishlistid)
        {
            WishList wishList = await AllWishlistsWithIncludes().SingleOrDefaultAsync(m => m.WishListId == wishlistid);
            if (wishList == null)
            {
                return NotFound();
            }

            wishList.Requests = new List<JoinTable_Requests>();

            await _context.SaveChangesAsync();

            return Ok(wishList.Friends);
        }

        [HttpPost("CreateRequest/{wishlistid}")]
        public async Task<IActionResult> CreateRequest([FromBody] User user, [FromRoute] int wishlistid)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var wishlist = await AllWishlistsWithIncludes().SingleOrDefaultAsync(m => m.WishListId == wishlistid);
            if (wishlist == null)
            {
                return NotFound();
            }

            wishlist.Requests.Add(new JoinTable_Requests() { UserId = user.UserId, WishlistId = wishlistid });
            
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}