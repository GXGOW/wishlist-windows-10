using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using WishlistBackend.Models;

namespace WishlistBackend.Controllers
{
    [Produces("application/json")]
    [Route("api/WishListItems")]
    public class WishListItemsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public WishListItemsController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpPost("changeBought/{id}")]
        public IActionResult ChangeBought([FromRoute] int id, [FromBody] int userId)
        {
            User user = _context.User.First(i => i.UserId == userId);
            Console.WriteLine("Testing");
            Console.WriteLine(id + " " + userId);

            WishListItem item = _context.WishListItem.First(w => w.WishListItemId == id);
            item.Bought = !item.Bought;
            item.BoughtBy = item.Bought ? user : null;
            try
            {
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return NotFound();
            }
            return Ok();
        }

        // GET: api/WishListItems
        [HttpGet]
        public IEnumerable<WishListItem> GetWishListItem()
        {
            return _context.WishListItem;
        }

        // GET: api/WishListItems/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetWishListItem([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var wishListItem = await _context.WishListItem.SingleOrDefaultAsync(m => m.WishListItemId == id);

            if (wishListItem == null)
            {
                return NotFound();
            }

            return Ok(wishListItem);
        }

        // PUT: api/WishListItems/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWishListItem([FromRoute] int id, [FromBody] WishListItem wishListItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != wishListItem.WishListItemId)
            {
                return BadRequest();
            }

            _context.Entry(wishListItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WishListItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/WishListItems
        [HttpPost]
        public async Task<IActionResult> PostWishListItem([FromBody] WishListItem wishListItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.WishListItem.Add(wishListItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetWishListItem", new { id = wishListItem.WishListItemId }, wishListItem);
        }

        [HttpPost("addItem/{id}")]
        public async Task<IActionResult> PostWishListItemAndAddToList([FromBody] WishListItem item, [FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _context.WishListItem.Add(item);
            var list = _context.WishList.SingleOrDefault(w => w.WishListId == id);
            list.Items.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetWishListItem", new { id = item.WishListItemId }, item);
        }

        // DELETE: api/WishListItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWishListItem([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var wishListItem = await _context.WishListItem.SingleOrDefaultAsync(m => m.WishListItemId == id);
            if (wishListItem == null)
            {
                return NotFound();
            }

            _context.WishListItem.Remove(wishListItem);
            await _context.SaveChangesAsync();

            return Ok(wishListItem);
        }

        private bool WishListItemExists(int id)
        {
            return _context.WishListItem.Any(e => e.WishListItemId == id);
        }
    }
}