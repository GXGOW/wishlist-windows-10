﻿using Newtonsoft.Json;
using Semestertaak_Whishlist.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Semestertaak_Whishlist
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Register : Page
    {
        public AppViewBackButtonVisibility BackButtonVisibility
        {
            get { return SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility; }
            set { SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = value; }
        }
        public Register()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            BackButtonVisibility = Frame.CanGoBack ? AppViewBackButtonVisibility.Visible : AppViewBackButtonVisibility.Collapsed;
            SystemNavigationManager.GetForCurrentView().BackRequested += BackRequested;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            SystemNavigationManager.GetForCurrentView().BackRequested -= BackRequested;
        }

        private void BackRequested(object sender, BackRequestedEventArgs e)
        {
            OnBackRequested(sender, e);
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            if (Frame.CanGoBack)
            {
                e.Handled = true;
                Frame.GoBack();
            }
        }

        private async void Register_Click(object sender, RoutedEventArgs e)
        {
            ErrorMessage.Text = string.Empty;
            if (EmailBox.Text == null || PasswordBox.Password == null || RepeatPasswordBox.Password == null)
            {
                ErrorMessage.Text = "Please fill in all required fields!";
            }
            else
            {
                if (PasswordBox.Password != RepeatPasswordBox.Password)
                {
                    ErrorMessage.Text = "Passwords do not match!";
                    PasswordBox.Password = string.Empty;
                    RepeatPasswordBox.Password = string.Empty;
                }
                else
                {
                    Button_Register.IsEnabled = false;
                    ProgressOverlay.Visibility = Visibility.Visible;
                    RegisterProgress.IsActive = true;
                    HttpClient client = new HttpClient();
                    User user = new User(EmailBox.Text,PasswordBox.Password);
                    EmailBox.Text = string.Empty;
                    PasswordBox.Password = string.Empty;
                    RepeatPasswordBox.Password = string.Empty;
                    var url = new Uri("http://localhost:2110/api/register");
                    var content = new StringContent(JsonConvert.SerializeObject(user).ToString(), Encoding.UTF8, "application/json");
                    var res = await client.PostAsync(url, content);
                    ProgressOverlay.Visibility = Visibility.Collapsed;
                    RegisterProgress.IsActive = false;
                    Button_Register.IsEnabled = true;
                    if (res.StatusCode != HttpStatusCode.OK)
                    {
                        User resUser = JsonConvert.DeserializeObject<User>(await res.Content.ReadAsStringAsync());
                        ContentDialog registerFailedDialog = new ContentDialog
                        {
                            Title = "Account creation failed",
                            Content = "Please try again later.",
                            CloseButtonText = "OK"
                        };
                        await registerFailedDialog.ShowAsync();
                    }
                    else
                    {
                        ContentDialog registerSuccesDialog = new ContentDialog
                        {
                            Title = "Account created successfully!",
                            Content = "You can now login to your new account!",
                            CloseButtonText = "Login"
                        };
                        await registerSuccesDialog.ShowAsync();
                        Frame.Navigate(typeof(MainPage));
                    }
                }
            }
        }

        private void CredentialBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter && Button_Register.IsEnabled)
            {
                Register_Click(sender, e);
            }
            Button_Register.IsEnabled = EmailBox.Text.Length > 10 && PasswordBox.Password.Length > 8 && RepeatPasswordBox.Password.Length > 8;
        }
    }
}
