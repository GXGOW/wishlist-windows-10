﻿using Newtonsoft.Json;
using Semestertaak_Whishlist.HelperClasses;
using Semestertaak_Whishlist.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Printing;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Semestertaak_Whishlist
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ListPage : Page
    {
        private User _user;
        private List<String> whishlist = new List<String>();
        private List<WishList> wishLists;
        private bool currentWishListOwn;

        private List<WishList> sharedLists;
        private WishList currentWishList;
        private List<WishListItem> currentWishListItems;

        private PrintHelper printHelper;

        private ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

        public ListPage()
        {
            this.InitializeComponent();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            this._user = e.Parameter as User;

            userName.Text = _user.Username;
            string email = _user.Username.Split('@').First().ToUpper();
            userPicture.Initials = $"{email[0]}{email[email.Length - 1]}";
            if (_user.ProfilePicture != null)
            {
                using (InMemoryRandomAccessStream ms = new InMemoryRandomAccessStream())
                {
                    // Writes the image byte array in an InMemoryRandomAccessStream
                    // that is needed to set the source of BitmapImage.
                    using (DataWriter writer = new DataWriter(ms.GetOutputStreamAt(0)))
                    {
                        writer.WriteBytes(_user.ProfilePicture);
                        writer.StoreAsync().GetResults();
                    }

                    var image = new BitmapImage();
                    image.SetSource(ms);
                    userPicture.ProfilePicture = image;
                }
            }

            HttpClient client = new HttpClient();
            await DisplayOwnWishlists();
            await DisplayWishlistsFriends();

            // Retrieve local settings
            try
            {
                var storedId = localSettings.Values[Settings.SELECTED];
                int selectedId = (int)storedId;
                WishList selected = wishLists.FirstOrDefault(wishlist => wishlist.WishListId == selectedId);
                if (selected != null)
                {
                    wishlistView.SelectedItem = selected;
                    ListOwner.Visibility = Visibility.Visible;
                }
                else
                {
                    selected = sharedLists.FirstOrDefault(wishlist => wishlist.WishListId == selectedId);
                    if (selected != null)
                    {
                        wishlistViewOther.SelectedItem = selected;
                        ListOwner.Visibility = Visibility.Visible;
                    }
                }
            } catch (Exception)
            {
            }

            if (!PrintManager.IsSupported())
            {
                buttonPDF.Visibility = Visibility.Collapsed;
            }
            printHelper = new PrintHelper(this);
            printHelper.RegisterForPrinting();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (printHelper != null)
            {
                printHelper.UnregisterForPrinting();
            }
        }

        private async Task DisplayOwnWishlists()
        {
            HttpClient client = new HttpClient();
            var json = await client.GetStringAsync(new Uri("http://localhost:2110/api/Users/" + _user.UserId.ToString() + "/OwnLists"));
            wishLists = JsonConvert.DeserializeObject<List<WishList>>(json);
            wishLists.Sort(delegate (WishList x, WishList y)
            {
                if (x.Deadline == null && y.Deadline == null) return 0;
                else if (x.Deadline == null) return -1;
                else if (y.Deadline == null) return 1;
                else return x.Deadline.CompareTo(y.Deadline);
            });
            this._user.OwnWishlists = wishLists;
            wishlistView.ItemsSource = new ObservableCollection<WishList>(wishLists);
            if(wishLists != null && wishLists.Count > 0)
            {
                Wishlists_Header.Visibility = Visibility.Visible;
            }
        }

        private async Task DisplayWishlistsFriends()
        {
            HttpClient client = new HttpClient();
            var json = await client.GetStringAsync(new Uri("http://localhost:2110/api/WishLists/OthersWishlistsOfUser/" + _user.UserId.ToString()));
            sharedLists = JsonConvert.DeserializeObject<List<WishList>>(json);
            this._user.WishlistsFriends = sharedLists;
            wishlistViewOther.ItemsSource = sharedLists;
            if (sharedLists != null && sharedLists.Count > 0)
            {
                SharedWishlists_Header.Visibility = Visibility.Visible;
            }
        }

        private void whishlistAdded(object sender, ItemClickEventArgs e)
        {
            items.ItemsSource = whishlist;
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            MenuSplitView.IsPaneOpen = !MenuSplitView.IsPaneOpen;
        }

        private async void LoadWishlist(object sender, SelectionChangedEventArgs e)
        {

            int selectedindex = ((ListView)sender).SelectedIndex;
            if (selectedindex != -1)
            {
                //Remove selection on wishlists of others 
                wishlistViewOther.SelectedItem = null;

                int id = wishLists[selectedindex].WishListId;
                localSettings.Values[Settings.SELECTED] = id;
                await LoadSomebodysWishlist(id, true);
                WishlistDetails.Visibility = Visibility.Visible;
                if (noWishlistSelected.Visibility == Visibility.Visible)
                    noWishlistSelected.Visibility = Visibility.Collapsed;
            }
        }

        private void ItemClicked(object sender, ItemClickEventArgs e)
        {
            WishListItem clickedItem = (WishListItem)e.ClickedItem;
            if (ItemPanel.DataContext != null &&
                ((WishListItem)ItemPanel.DataContext).WishListItemId == clickedItem.WishListItemId && 
               WishlistOverview.IsPaneOpen)
            {
                WishlistOverview.IsPaneOpen = false;
                ItemPanel.DataContext = null;
            }
            else
            {
                ItemPanel.DataContext = clickedItem;
                WishlistOverview.IsPaneOpen = true;
                DeleteButton.Visibility = currentWishListOwn ? Visibility.Visible : Visibility.Collapsed;
                BoughtBy.Visibility = currentWishListOwn ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        private void AddItem_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(AddItemPage), new Object[] { _user, currentWishList });
        }

        private async void AddList(object sender, RoutedEventArgs e)
        {
            AddWishlist addWishlist = new AddWishlist();
            await addWishlist.ShowAsync();
            if (addWishlist.Result == "Success")
            {
                WishList list = addWishlist.newList;
                HttpClient client = new HttpClient();
                var uri = "http://localhost:2110/api/wishlists/addToUser/" + _user.UserId.ToString();
                var content = new StringContent(JsonConvert.SerializeObject(list), Encoding.UTF8, "application/json");
                var res = await client.PostAsync(new Uri(uri), content);
                var json = await res.Content.ReadAsStringAsync();
                var listInDb = JsonConvert.DeserializeObject<WishList>(json);
                wishLists.Add(listInDb);
                wishLists.Sort(delegate (WishList x, WishList y)
                {
                    if (x.Deadline == null && y.Deadline == null) return 0;
                    else if (x.Deadline == null) return -1;
                    else if (y.Deadline == null) return 1;
                    else return x.Deadline.CompareTo(y.Deadline);
                });
                wishlistView.ItemsSource = new ObservableCollection<WishList>(wishLists);
                Wishlists_Header.Visibility = Visibility.Visible;
            }
        }
        private async void LoadWishlistOthers(object sender, SelectionChangedEventArgs e)
        {
            int selectedindex = ((ListView)sender).SelectedIndex;
            if (selectedindex != -1)
            {
                //Remove selection on list of own wishlists
                wishlistView.SelectedItem = null;

                int id = sharedLists[selectedindex].WishListId;
                localSettings.Values[Settings.SELECTED] = id;
                await LoadSomebodysWishlist(id, false);
                WishlistDetails.Visibility = Visibility.Visible;
                if (noWishlistSelected.Visibility == Visibility.Visible)
                    noWishlistSelected.Visibility = Visibility.Collapsed;
            }
        }

        private async Task LoadSomebodysWishlist(int id, Boolean userIsOwner)
        {
            currentWishListOwn = userIsOwner;
            HttpClient client = new HttpClient();
            var json = await client.GetStringAsync(new Uri("http://localhost:2110/api/WishLists/" + id.ToString()));
            currentWishList = JsonConvert.DeserializeObject<WishList>(json);
            if(currentWishList.Deadline == DateTime.Today)
            {
                Text_DueDate.Text = "Deadline: Today!";
            }
            else if ((DateTime.Today - currentWishList.Deadline).TotalDays == -1)
            {
                Text_DueDate.Text = "Deadline: Tomorrow!";
            }
            else if (currentWishList.Deadline < DateTime.Now)
            {
                Text_DueDate.Text = "Deadline: Overdue!";
            }
            else Text_DueDate.Text = $"Deadline: {currentWishList.Deadline.ToString("dd/MM/yyyy")}";
            currentWishListItems = currentWishList.Items;
            
            if (userIsOwner)
            {
                buttonAddItem.Visibility = Visibility.Visible;
                buttonInviteFriends.Visibility = Visibility.Visible;
                Owner.Text = _user.Username;
                currentWishListItems.ForEach(l => l.Bought = false);

                await GetInvites();
                await GetRequests();
            }
            else
            {
                buttonAddItem.Visibility = Visibility.Collapsed;
                buttonInviteFriends.Visibility = Visibility.Collapsed;

                var jsonOwner = await client.GetStringAsync(new Uri("http://localhost:2110/api/Users/OwnerOfWishlist/" + id.ToString()));
                var owner = JsonConvert.DeserializeObject<User>(jsonOwner);
                Owner.Text = owner.Username;
            }
            var groups = from w in currentWishListItems group w by w.Category.Name;
            cvs.Source = groups;

            await GetFriends();
            
            ListOwner.Visibility = Visibility.Visible;
        }

        private async Task GetRequests()
        {
            HttpClient client = new HttpClient();
            var jsonRequests = await client.GetStringAsync(new Uri("http://localhost:2110/api/Users/RequestedUsersOfWishlist/" + currentWishList.WishListId.ToString()));
            var requests = JsonConvert.DeserializeObject<List<User>>(jsonRequests);
            currentWishList.Requests = requests;

            RequestedFriends.ItemsSource = requests;
            if (requests != null && requests.Count > 0)
            {
                RequestedFriendsOverview.Visibility = Visibility.Visible;
            }
            else
            {
                RequestedFriendsOverview.Visibility = Visibility.Collapsed;
            }
        }

        private async Task GetInvites()
        {
            HttpClient client = new HttpClient();
            var jsonInvites = await client.GetStringAsync(new Uri("http://localhost:2110/api/Users/InvitedUsersOfWishlist/" + currentWishList.WishListId.ToString()));
            var invites = JsonConvert.DeserializeObject<List<User>>(jsonInvites);
            currentWishList.Invites = invites;

            InvitedFriends.ItemsSource = invites;
            if (invites != null && invites.Count > 0)
            {
                InvitedFriendsOverview.Visibility = Visibility.Visible;
            }
            else
            {
                InvitedFriendsOverview.Visibility = Visibility.Collapsed;
            }
        }

        private async Task GetFriends()
        {
            HttpClient client = new HttpClient();
            var jsonFriends = await client.GetStringAsync(new Uri("http://localhost:2110/api/Users/FriendsOfWishlist/" + currentWishList.WishListId.ToString()));
            var friends = JsonConvert.DeserializeObject<List<User>>(jsonFriends);
            currentWishList.Friends = friends;

            Friends.ItemsSource = friends;
            if (friends != null && friends.Count > 0)
            {
                FriendsOverview.Visibility = Visibility.Visible;
            }
            else
            {
                FriendsOverview.Visibility = Visibility.Collapsed;
            }
        }

        private void InviteFriends_Click(object sender, RoutedEventArgs e)
        {
            var param = new Object[]{
                _user,
                currentWishList
            };
            this.Frame.Navigate(typeof(InviteFriends), param);
        }

        private async void ChangeBought(object sender, RoutedEventArgs e)
        {
            if (!currentWishListOwn)
            {
                CheckBox item = (CheckBox)sender;
                StackPanel panel = (StackPanel)item.Parent;
                var items = panel.Children;
                var itemTB = (TextBlock)items[2];
                int itemId = Int32.Parse(itemTB.Text);
                if (itemId != -1)
                {
                    HttpClient client = new HttpClient();
                    var uri = "http://localhost:2110/api/WishListItems/changeBought/" + itemId;
                    var content = new StringContent(JsonConvert.SerializeObject(_user.UserId), Encoding.UTF8, "application/json");
                    var json = await client.PostAsync(new Uri(uri), content);
                    WishListItem wishListItem = currentWishListItems.Find(i => i.WishListItemId == itemId);
                    wishListItem.BoughtBy = wishListItem.Bought ? _user : null;
                }
            }
            else
            {
                (sender as CheckBox).IsChecked = false;
            }
        }

        private void Button_View_Invitations(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(InvitesAndRequests), _user);
        }

        private void Button_CreateRequest(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SendRequest), _user);
        }

        private async void ExportPDF_Click(object sender, RoutedEventArgs e)
        {
            IEnumerable<IGrouping<string, WishListItem>> groups = from w in currentWishListItems group w by w.Category.Name;
            PrintPage printPage = new PrintPage();
            Paragraph titleParagraph = new Paragraph();
            Run titleRun = new Run();
            titleParagraph.FontSize = 36;
            titleParagraph.FontWeight = FontWeights.ExtraBold;
            titleRun.Text = currentWishList.Name;
            titleParagraph.Inlines.Add(titleRun);
            printPage.AddToPage(titleParagraph);
            Paragraph ownerParagraph = new Paragraph();
            Run ownerRun = new Run();
            ownerParagraph.FontSize = 25;
            ownerParagraph.FontWeight = FontWeights.Bold;
            ownerRun.Text = $"Owned by: {Owner.Text}";
            ownerParagraph.Inlines.Add(ownerRun);
            printPage.AddToPage(ownerParagraph);
            foreach (IGrouping<string, WishListItem> group in groups)
            {
                Paragraph groupParagraph = new Paragraph();
                Run groupRun = new Run();
                groupParagraph.FontSize = 20;
                groupParagraph.FontWeight = FontWeights.Bold;
                groupRun.Text = group.Key + ":";
                groupParagraph.Inlines.Add(groupRun);
                printPage.AddToPage(groupParagraph);
                foreach (WishListItem item in group)
                {
                    Paragraph itemParagraph = new Paragraph();
                    Run itemRun = new Run();
                    itemRun.Text = $"{(item.Bought ? "☑" : "☐")} {item.Name}";
                    itemParagraph.Inlines.Add(itemRun);
                    printPage.AddToPage(itemParagraph);
                }
            }
        
            printHelper.PreparePrintContent(printPage);
            await printHelper.ShowPrintUIAsync();
        }

        private void Profile_Clicked(object sender, RoutedEventArgs e)
        {
            FlyoutBase.ShowAttachedFlyout((FrameworkElement)sender);
        }

        private async void Button_Logout(object sender, RoutedEventArgs e)
        {
            try
            {
                string jwt = localSettings.Values[Settings.JWT].ToString();
                HttpClient client = new HttpClient();
                Uri url = new Uri("http://localhost:2110/api/logout");
                var content = new StringContent(JsonConvert.SerializeObject(jwt), Encoding.UTF8, "application/json");
                await client.PostAsync(url, content);
            }
            catch (NullReferenceException)
            {
            }
            finally
            {
                localSettings.Values.Remove(Settings.SELECTED);
                localSettings.Values.Remove(Settings.JWT);
                Frame.Navigate(typeof(MainPage), null, new SuppressNavigationTransitionInfo());
            }
        }

        private async void AddNewCategory(object sender, RoutedEventArgs e)
        {
            AddCategory addCategory = new AddCategory();
            await addCategory.ShowAsync();
            if (addCategory.Result == "Success")
            {
                Category category = addCategory.Category;
                category.UserId = _user.UserId;
                HttpClient client = new HttpClient();
                var uri = "http://localhost:2110/api/Categories";
                var content = new StringContent(JsonConvert.SerializeObject(category), Encoding.UTF8, "application/json");
                await client.PostAsync(new Uri(uri), content);
                //Add backend method for adding Category. And change the Category model to allow user connection
                // Next to that change the addItempage to get all Categorys for a certain user.
            }
        }

        private void Button_Settings(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(UserSetting), _user);
        }

        private async void Requests_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = ((ListBox)sender).SelectedIndex;
            if (index != -1)
            {
                await AcceptOrReject(index);
            }
        }

        private async void AcceptAllRequests_Click(object sender, RoutedEventArgs e)
        {
            await AcceptOrReject(-1);
        }

        private async System.Threading.Tasks.Task AcceptOrReject(int index)
        {
            ContentDialog acceptOrReject = new ContentDialog()
            {
                Title = "Accept or reject " + ((index !=-1)? "the request?" : "the requests?"),
                Content = (index != -1) ? "Would you like to accept or reject the request?" : "Would you like to accept or reject all the requests?",
                PrimaryButtonText = "Accept",
                SecondaryButtonText = "Reject"
            };

            ContentDialogResult result = await acceptOrReject.ShowAsync();
            if (result == ContentDialogResult.Primary)
            {
                //Accept or reject invite
                await AcceptOrRejectInBackend(index, "Accept");
            }
            else if(result ==ContentDialogResult.Secondary)
            {
                await AcceptOrRejectInBackend(index, "Reject");
            }
        }

        private async System.Threading.Tasks.Task AcceptOrRejectInBackend(int index, String acceptOrReject)
        {
            //Process invite in backend
            HttpClient client = new HttpClient();
            var res = new HttpResponseMessage();
            if (index != -1)
            {
                User request = currentWishList.Requests[index];
                
                var uri = "http://localhost:2110/api/Wishlists/"+ acceptOrReject+"RequestToWishlist/ " + currentWishList.WishListId.ToString();
                var content = new StringContent(JsonConvert.SerializeObject(request.UserId), Encoding.UTF8, "application/json");
                res = await client.PostAsync(new Uri(uri), content);
            }
            else
            {
                var uri = "http://localhost:2110/api/Wishlists/" + acceptOrReject + "AllRequests/" + currentWishList.WishListId.ToString();
                res = await client.GetAsync(new Uri(uri));

            }
            
            //Update list of invites and show message
            if (res.StatusCode == HttpStatusCode.OK)
            {
                await GetFriends();
                await GetInvites();
                await GetRequests();
                ContentDialog succes = new ContentDialog()
                {
                    Title = acceptOrReject + "ed",
                    Content = (index !=-1? "The request": "All the requests") + " to collaborate to this wishlist "+ (index != -1 ? "has" : "have") + " been accepted.",
                    CloseButtonText = "OK"
                };
                await succes.ShowAsync();
            }
            else
            {
                ContentDialog fail = new ContentDialog()
                {
                    Title = acceptOrReject + "ing request" + (index != -1 ? "" : "s") + " failed",
                    Content = "Something went wrong. Please try again.",
                    CloseButtonText = "OK"
                };
                await fail.ShowAsync();
            }
        }

        private async void DeleteItem(object sender, RoutedEventArgs e)
        {
            if (currentWishListOwn)
            {
                WishListItem currentItem = (WishListItem)ItemPanel.DataContext;
                HttpClient client = new HttpClient();
                var uri = "http://localhost:2110/api/WishListItems/" + currentItem.WishListItemId.ToString();
                var res = await client.DeleteAsync(new Uri(uri));
                if (res.IsSuccessStatusCode)
                {
                    currentWishListItems.RemoveAll(i => i.WishListItemId == currentItem.WishListItemId);
                    var groups = from w in currentWishListItems group w by w.Category.Name;
                    cvs.Source = groups;
                    WishlistOverview.IsPaneOpen = false;
                }
                else
                {
                    ContentDialog error = new ContentDialog()
                    {
                        Title = "Cannot remove item",
                        Content = $"Error in deleting this item: {res.ReasonPhrase}",
                        CloseButtonText = "Close"
                    };
                    await error.ShowAsync();
                }
            }
            else
            {
                ContentDialog error = new ContentDialog()
                {
                    Title = "Cannot remove item",
                    Content = "This item cannot be removed because you do not own the wishlist this item is part of.",
                    CloseButtonText = "Close"
                };
                await error.ShowAsync();
            }
        }
    }
}
