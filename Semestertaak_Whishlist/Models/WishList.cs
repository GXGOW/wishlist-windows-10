﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semestertaak_Whishlist.Models
{
    public class WishList
    {
        public int WishListId { get; set; }
        public List<User> Friends { get; set; }
        public List<User> Invites { get; set; }
        public List<User> Requests { get; set; }
        public List<WishListItem> Items { get; set; }
        public String Name { get; set; }
        public DateTime Deadline { get; set; }

        public WishList()
        {
            this.Deadline = DateTime.Now;
        }

        public void AddItemPage(WishListItem item)
        {
            if(Items == null)
            {
                Items = new List<WishListItem>();
            }
            this.Items.Add(item);
        }
    }
}
