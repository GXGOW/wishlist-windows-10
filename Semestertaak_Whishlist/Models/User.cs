﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semestertaak_Whishlist.Models
{
    public class User
    {
        public int UserId { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public List<WishList> OwnWishlists { get; set; }
        public List<WishList> WishlistsFriends { get; set; }
        public List<WishList> Invites { get; set; }
        public List<WishList> Requests { get; set; }
        public byte[] ProfilePicture { get; set; }

        public User(String username, String password)
        {
            this.Username = username;
            this.Password = password;
        }

        public User() { }
        
    }
}
