﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semestertaak_Whishlist.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        public String Name { get; set; }
        public int? UserId { get; set; }
    }
}
