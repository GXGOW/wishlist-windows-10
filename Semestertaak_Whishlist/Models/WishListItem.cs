﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace Semestertaak_Whishlist.Models
{
    public class WishListItem : INotifyPropertyChanged
    {
        public int WishListItemId { get; set; }
        private String _name;
        public int CategoryId { get; set; }

        public Category Category { get; set; }

        public String Name
        {
            get { return _name; }
            set { _name = value; NotifyPropertyChanged("Name"); }
        }

        private String _description;

        public String Description
        {
            get { return _description; }
            set { _description = value; NotifyPropertyChanged("Description"); }
        }


        private bool _isBought;
        public bool Bought
        {
            get { return _isBought; }
            set { _isBought = value; NotifyPropertyChanged("IsBought"); }
        }
        private byte[] _imgData;

        public byte[] image
        {
            get { return _imgData; }
            set { _imgData = value; NotifyPropertyChanged("image"); }
        }

        private User _boughtBy;

        public User BoughtBy
        {
            get { return _boughtBy; }
            set { _boughtBy = value; NotifyPropertyChanged("BoughtBy"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
