﻿using Newtonsoft.Json;
using Semestertaak_Whishlist.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Windows.ApplicationModel.Core;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Semestertaak_Whishlist
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddItemPage : Page
    {
        private User user;
        private WishListItem newItem;
        private List<Category> cats;
        public AppViewBackButtonVisibility BackButtonVisibility
        {
            get { return SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility; }
            set { SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = value; }
        }
        public AddItemPage()
        {
            this.InitializeComponent();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            BackButtonVisibility = Frame.CanGoBack ? AppViewBackButtonVisibility.Visible : AppViewBackButtonVisibility.Collapsed;
            SystemNavigationManager.GetForCurrentView().BackRequested += BackRequested;
            Object[] param = e.Parameter as Object[];
            user = param[0] as User;
            newItem = new WishListItem();
            MainSplitView.DataContext = newItem;
            HttpClient client = new HttpClient();
            int index = user.OwnWishlists.FindIndex(item => item.WishListId == (param[1] as WishList).WishListId);
            WishListSelect.ItemsSource = user.OwnWishlists;
            WishListSelect.SelectedIndex = index;
            var json = await client.GetStringAsync(new Uri("http://localhost:2110/api/Categories/byUser/" + user.UserId.ToString()));
            cats = JsonConvert.DeserializeObject<List<Category>>(json);
            CategoryCombobox.ItemsSource = cats;
        }

        private async void Button_Upload(object sender, RoutedEventArgs e)
        {
            FileOpenPicker picker = new FileOpenPicker();
            picker.FileTypeFilter.Add(".bmp");
            picker.FileTypeFilter.Add(".png");
            picker.FileTypeFilter.Add(".jpg");
            StorageFile file = await picker.PickSingleFileAsync();
            if (file != null)
            {
                using (var inputStream = await file.OpenSequentialReadAsync())
                {
                    var readStream = inputStream.AsStreamForRead();
                    byte[] buffer = new byte[readStream.Length];
                    await readStream.ReadAsync(buffer, 0, buffer.Length);
                    newItem.image = buffer;
                }
            }
        }

        private async void AddItem(object sender, RoutedEventArgs e)
        {
            if (newItem.Name == null || newItem.Name == "" || newItem.Description == "" || newItem.Description == null || newItem.CategoryId == 0)
            {
                ContentDialog warning = new ContentDialog
                {
                    Title = "Form not filled in correctly",
                    Content = "Name, description and Category have to be filled in",
                    CloseButtonText = "Close"
                };
                await warning.ShowAsync();
            }
            else if (WishListSelect.SelectedValue == null)
            {
                ContentDialog warning = new ContentDialog
                {
                    Title = "Form not filled in correctly",
                    Content = "You did not select a wishlist to add this item too",
                    CloseButtonText = "Close"
                };
                await warning.ShowAsync();
            }
            else
            {
                WishList list = (WishList)WishListSelect.SelectedValue;
                list.AddItemPage(newItem);
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var uri = "http://localhost:2110/api/WishListitems/addItem/" + list.WishListId.ToString();
                var content = new StringContent(JsonConvert.SerializeObject(newItem), Encoding.UTF8, "application/json");
                await client.PostAsync(new Uri(uri), content);
                Frame.Navigate(typeof(ListPage), user);
            }
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            if(Frame.CanGoBack)
            {
                Frame.GoBack();
            }
            else
            {
                CoreApplication.Exit();
            }
        }

        private void CategorySelect(object sender, SelectionChangedEventArgs e)
        {
            int index = ((ComboBox)sender).SelectedIndex;
            newItem.CategoryId = cats[index].CategoryId;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            SystemNavigationManager.GetForCurrentView().BackRequested -= BackRequested;
        }

        private void BackRequested(object sender, BackRequestedEventArgs e)
        {
            OnBackRequested(sender, e);
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            if (Frame.CanGoBack)
            {
                e.Handled = true;
                Frame.GoBack();
            }
        }

        private void Button_Toggle_Click(object sender, RoutedEventArgs e)
        {
            MainSplitView.IsPaneOpen = true;
        }
    }
}
