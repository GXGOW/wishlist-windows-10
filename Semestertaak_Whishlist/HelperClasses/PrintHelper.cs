﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Graphics.Printing;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Printing;

namespace Semestertaak_Whishlist.HelperClasses
{
    public class PrintHelper
    {
        protected double ApplicationContentMarginLeft = 0.075;

        protected double ApplicationContentMarginTop = 0.03;

        protected PrintDocument printDocument;

        protected IPrintDocumentSource printDocumentSource;

        internal List<UIElement> printPreviewPages;

        protected event EventHandler PreviewPagesCreated;

        protected FrameworkElement firstPage;

        protected Page scenarioPage;

        protected Canvas PrintCanvas
        {
            get
            {
                return scenarioPage.FindName("PrintCanvas") as Canvas;
            }
        }

        public PrintHelper(Page scenarioPage)
        {
            this.scenarioPage = scenarioPage;
            printPreviewPages = new List<UIElement>();
        }

        public virtual void RegisterForPrinting()
        {
            try
            {
                printDocument = new PrintDocument();
                printDocumentSource = printDocument.DocumentSource;
                printDocument.Paginate += CreatePrintPreviewPages;
                printDocument.GetPreviewPage += GetPrintPreviewPage;
                printDocument.AddPages += AddPrintPages;

                PrintManager printMan = PrintManager.GetForCurrentView();
                printMan.PrintTaskRequested += PrintTaskRequested;
            } catch(Exception ex)
            {

            }
        }

        public virtual void UnregisterForPrinting()
        {
            if (printDocument == null)
            {
                return;
            }

            printDocument.Paginate -= CreatePrintPreviewPages;
            printDocument.GetPreviewPage -= GetPrintPreviewPage;
            printDocument.AddPages -= AddPrintPages;

            PrintManager printMan = PrintManager.GetForCurrentView();
            printMan.PrintTaskRequested -= PrintTaskRequested;

            PrintCanvas.Children.Clear();
        }

        public async Task ShowPrintUIAsync()
        {
            try
            {
                await PrintManager.ShowPrintUIAsync();
            }
            catch (Exception e)
            {
                ContentDialog error = new ContentDialog()
                {
                    Title = "Printing failed",
                    Content = "Error printing: " + e.Message + ", hr=" + e.HResult,
                    CloseButtonText = "OK"
                };
                await error.ShowAsync();
            }
        }

        public virtual void PreparePrintContent(Page page)
        {
            if (firstPage == null)
            {
                firstPage = page;
            }

            PrintCanvas.Children.Add(firstPage);
            PrintCanvas.InvalidateMeasure();
            PrintCanvas.UpdateLayout();
        }

        protected virtual void PrintTaskRequested(PrintManager sender, PrintTaskRequestedEventArgs e)
        {
            PrintTask printTask = null;
            printTask = e.Request.CreatePrintTask("C# Printing SDK Sample", sourceRequested =>
            {
                printTask.Completed += async (s, args) =>
                {
                    if (args.Completion == PrintTaskCompletion.Failed)
                    {
                        await scenarioPage.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
                        {
                            ContentDialog error = new ContentDialog()
                            {
                                Title = "Printing failed",
                                Content = "Failed to print.",
                                CloseButtonText = "OK"
                            };
                            await error.ShowAsync();
                        });
                    }
                };

                sourceRequested.SetSource(printDocumentSource);
            });
        }
        protected virtual void CreatePrintPreviewPages(object sender, PaginateEventArgs e)
        {
            lock (printPreviewPages)
            {
                printPreviewPages.Clear();

                PrintCanvas.Children.Clear();

                RichTextBlockOverflow lastRTBOOnPage;

                PrintTaskOptions printingOptions = ((PrintTaskOptions)e.PrintTaskOptions);

                PrintPageDescription pageDescription = printingOptions.GetPageDescription(0);

                lastRTBOOnPage = AddOnePrintPreviewPage(null, pageDescription);

                while (lastRTBOOnPage.HasOverflowContent && lastRTBOOnPage.Visibility == Windows.UI.Xaml.Visibility.Visible)
                {
                    lastRTBOOnPage = AddOnePrintPreviewPage(lastRTBOOnPage, pageDescription);
                }

                if (PreviewPagesCreated != null)
                {
                    PreviewPagesCreated.Invoke(printPreviewPages, null);
                }

                PrintDocument printDoc = (PrintDocument)sender;

                printDoc.SetPreviewPageCount(printPreviewPages.Count, PreviewPageCountType.Intermediate);
            }
        }
        protected virtual void GetPrintPreviewPage(object sender, GetPreviewPageEventArgs e)
        {
            PrintDocument printDoc = (PrintDocument)sender;
            printDoc.SetPreviewPage(e.PageNumber, printPreviewPages[e.PageNumber - 1]);
        }
        protected virtual void AddPrintPages(object sender, AddPagesEventArgs e)
        {
            for (int i = 0; i < printPreviewPages.Count; i++)
            {
                printDocument.AddPage(printPreviewPages[i]);
            }

            PrintDocument printDoc = (PrintDocument)sender;

            printDoc.AddPagesComplete();
        }

        protected virtual RichTextBlockOverflow AddOnePrintPreviewPage(RichTextBlockOverflow lastRTBOAdded, PrintPageDescription printPageDescription)
        {
            FrameworkElement page;

            RichTextBlockOverflow textLink;

            if (lastRTBOAdded == null)
            {
                page = firstPage;
            }
            else
            {
                page = new ContinuationPage(lastRTBOAdded);
            }

            page.Width = printPageDescription.PageSize.Width;
            page.Height = printPageDescription.PageSize.Height;

            Grid printableArea = (Grid)page.FindName("PrintableArea");

            double marginWidth = Math.Max(printPageDescription.PageSize.Width - printPageDescription.ImageableRect.Width, printPageDescription.PageSize.Width * ApplicationContentMarginLeft * 2);
            double marginHeight = Math.Max(printPageDescription.PageSize.Height - printPageDescription.ImageableRect.Height, printPageDescription.PageSize.Height * ApplicationContentMarginTop * 2);

            printableArea.Width = firstPage.Width - marginWidth;
            printableArea.Height = firstPage.Height - marginHeight;

           PrintCanvas.Children.Add(page);
            PrintCanvas.InvalidateMeasure();
            PrintCanvas.UpdateLayout();

            textLink = (RichTextBlockOverflow)page.FindName("ContinuationPageLinkedContainer");

            printPreviewPages.Add(page);

            return textLink;
        }
    }
}
