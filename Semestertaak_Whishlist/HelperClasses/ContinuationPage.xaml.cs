﻿using Windows.UI.Xaml.Controls;

namespace Semestertaak_Whishlist.HelperClasses
{
    public sealed partial class ContinuationPage : Page
    {
        public ContinuationPage(RichTextBlockOverflow textLinkContainer)
        {
            InitializeComponent();
            textLinkContainer.OverflowContentTarget = ContinuationPageLinkedContainer;
        }
    }
}
