﻿using Newtonsoft.Json;
using Semestertaak_Whishlist.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using System.Net;
using Windows.UI.Xaml;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Semestertaak_Whishlist
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class InvitesAndRequests : Page
    {
        private User _user;
        private List<WishList> invitesUsers;
        private List<User> requests;

        public AppViewBackButtonVisibility BackButtonVisibility
        {
            get { return SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility; }
            set { SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = value; }
        }
        public InvitesAndRequests()
        {
            this.InitializeComponent();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            BackButtonVisibility = Frame.CanGoBack ? AppViewBackButtonVisibility.Visible : AppViewBackButtonVisibility.Collapsed;
            SystemNavigationManager.GetForCurrentView().BackRequested += BackRequested;
            this._user = e.Parameter as User;
            await GetInvitations();
            await GetRequests();
        }

        private async System.Threading.Tasks.Task GetInvitations()
        {
            HttpClient client = new HttpClient();
            var json = await client.GetStringAsync(new Uri("http://localhost:2110/api/WishLists/InvitedWishlistsOfUser/" + _user.UserId.ToString()));
            invitesUsers = JsonConvert.DeserializeObject<List<WishList>>(json);
            Invites.ItemsSource = invitesUsers;
        }

        private async System.Threading.Tasks.Task GetRequests()
        {
            requests = new List<User>();
            HttpClient client = new HttpClient();
            var jsonOwnLists = await client.GetStringAsync(new Uri("http://localhost:2110/api/Users/" + _user.UserId.ToString() + "/OwnLists"));
            var wishLists = JsonConvert.DeserializeObject<List<WishList>>(jsonOwnLists);

            foreach (var wishList in wishLists)
            {
                var jsonUser = await client.GetStringAsync(new Uri("http://localhost:2110/api/Users/RequestedUsersOfWishlist/" + wishList.WishListId.ToString()));
                var usersRequest = JsonConvert.DeserializeObject<List<User>>(jsonUser);
                foreach (var userRequest in usersRequest)
                {
                    //Only if the user isn't already in the list of requestedUsers
                    if (requests.Find(u => u.UserId == userRequest.UserId) == null)
                    {
                        requests.Add(userRequest);
                    }
                }
            }

            RequestsOwnLists.ItemsSource = requests;
        }

        private async void Invites_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = ((ListBox)sender).SelectedIndex;
            if (index != -1)
            {
                await AcceptOrReject("invite", index);
            }
        }

        private async System.Threading.Tasks.Task AcceptOrReject(String inviteOrRequest, int index)
        {
            ContentDialog acceptOrReject = new ContentDialog()
            {
                Title = "Accept or reject "+inviteOrRequest,
                Content = (inviteOrRequest == "invite") ? "Would you like to accept or reject the invitation?" : "Would you like to accept or reject all the requests of the user?",
                PrimaryButtonText = "Accept",
                SecondaryButtonText = "Reject"
            };

            ContentDialogResult result = await acceptOrReject.ShowAsync();
            if (result == ContentDialogResult.Primary)
            {
                //Accept or reject invite
                await AcceptOrRejectInBackend(inviteOrRequest, index, "Accept");
            }
            else if (result == ContentDialogResult.Secondary)
            {
                await AcceptOrRejectInBackend(inviteOrRequest, index, "Reject");
            }
        }

        private async System.Threading.Tasks.Task AcceptOrRejectInBackend(String inviteOrRequest, int index, String acceptOrReject)
        {

            //Process invite in backend
            HttpClient client = new HttpClient();
            var res = new HttpResponseMessage();
            if (inviteOrRequest == "invite")
            {
                WishList invite = invitesUsers[index];
                var uri = "http://localhost:2110/api/Users/" + acceptOrReject + "InviteToWishlist/" + _user.UserId.ToString();
                var content = new StringContent(JsonConvert.SerializeObject(invite.WishListId), Encoding.UTF8, "application/json");
                res = await client.PostAsync(new Uri(uri), content);
            }
            else
            {
                User request = requests[index];
                var uri = "http://localhost:2110/api/Users/" + acceptOrReject + "AllRequestsFromUser/" + _user.UserId.ToString();
                var content = new StringContent(JsonConvert.SerializeObject(request.UserId), Encoding.UTF8, "application/json");
                res = await client.PostAsync(new Uri(uri), content);
            }


            //Update list of invites and show message
            if (res.StatusCode == HttpStatusCode.OK)
            {
                String partOfContent = inviteOrRequest;
                if (inviteOrRequest == "invite")
                {
                    await GetInvitations();
                    partOfContent += " has";
                }
                else
                {
                    await GetRequests();
                    partOfContent += "s have";
                }
                
                ContentDialog succes = new ContentDialog()
                {
                    Title = acceptOrReject + "ed",
                    Content = "The " + partOfContent + " been " + acceptOrReject.ToLower() + "ed",
                    CloseButtonText = "OK",
                };
                await succes.ShowAsync();
            }
            else
            {
                ContentDialog fail = new ContentDialog()
                {
                    Title = acceptOrReject + "ing " + inviteOrRequest + " failed",
                    Content = "Something went wrong. Please try again.",
                    CloseButtonText = "OK"
                };
                await fail.ShowAsync();
            }
        }

        private async void RequestsOwnLists_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = ((ListBox)sender).SelectedIndex;
            if (index != -1)
            {
                await AcceptOrReject("request", index);
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            SystemNavigationManager.GetForCurrentView().BackRequested -= BackRequested;
        }

        private void BackRequested(object sender, BackRequestedEventArgs e)
        {
            OnBackRequested(sender, e);
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            if (Frame.CanGoBack)
            {
                e.Handled = true;
                Frame.GoBack();
            }
        }
    }
}
