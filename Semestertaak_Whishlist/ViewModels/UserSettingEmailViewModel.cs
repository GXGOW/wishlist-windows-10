﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semestertaak_Whishlist.ViewModels
{
    class UserSettingEmailViewModel
    {
        public String NewEmail { get; set; }
        public String RepeatEmail { get; set; }
    }
}
