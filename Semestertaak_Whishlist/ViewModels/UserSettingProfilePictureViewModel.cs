﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semestertaak_Whishlist.ViewModels
{
    class UserSettingProfilePictureViewModel : INotifyPropertyChanged
    {
        public byte[] _imgData;
        public byte[] image
        {
            get { return _imgData; }
            set { _imgData = value; NotifyPropertyChanged("image"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
