﻿using Newtonsoft.Json;
using Semestertaak_Whishlist.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Semestertaak_Whishlist
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SendRequest : Page
    {
        private User user;
        private List<User> PeopleToSendRequests;
        private List<WishList> wishlistsToRequest;

        private ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

        public AppViewBackButtonVisibility BackButtonVisibility
        {
            get { return SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility; }
            set { SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = value; }
        }

        public SendRequest()
        {
            this.InitializeComponent();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            BackButtonVisibility = Frame.CanGoBack ? AppViewBackButtonVisibility.Visible : AppViewBackButtonVisibility.Collapsed;
            SystemNavigationManager.GetForCurrentView().BackRequested += BackRequested;
            this.user = e.Parameter as User;

            HttpClient client = new HttpClient();
            var json = await client.GetStringAsync(new Uri("http://localhost:2110/api/Users/"));
            var usersList = JsonConvert.DeserializeObject<List<User>>(json);

            usersList = usersList.Where(userOfList => userOfList.UserId != user.UserId).ToList();
            if (usersList.Count != 0)
            {
                PeopleToSendRequests = usersList;
                Friends.ItemsSource = PeopleToSendRequests;
                NoFriendsOverview.Visibility = Visibility.Collapsed;
                FriendsOverview.Visibility = Visibility.Visible;
            }
            else
            {
                NoFriendsOverview.Visibility = Visibility.Visible;
                FriendsOverview.Visibility = Visibility.Collapsed;
            }
        }

        private void txtFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            String filter = txtFilter.Text;
            List<User> filteredList = PeopleToSendRequests.FindAll(u => u.Username.IndexOf(filter, StringComparison.OrdinalIgnoreCase) >= 0);
            Friends.ItemsSource = filteredList;
        }

        private async void Friends_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int selectedindex = ((ListView)sender).SelectedIndex;
            if (selectedindex != -1)
            {
                int friendid = PeopleToSendRequests[selectedindex].UserId;
                localSettings.Values[Settings.SELECTED] = friendid;
                await GetWishlistsOfSelectedFriend();
            }
        }

        private async Task GetWishlistsOfSelectedFriend()
        {
            int friendid = (int)localSettings.Values[Settings.SELECTED];
            //Get wishlists of friend
            HttpClient client = new HttpClient();
            var json = await client.GetStringAsync(new Uri("http://localhost:2110/api/Users/" + friendid.ToString() + "/OwnLists"));
            var wishlists = JsonConvert.DeserializeObject<List<WishList>>(json);
            wishlistsToRequest = new List<WishList>();

            //Remove wishlists where the user is already friends or has an open request.
            foreach (var wishList in wishlists)
            {
                var jsonFriends = await client.GetStringAsync(new Uri("http://localhost:2110/api/Users/FriendsOfWishlist/" + wishList.WishListId.ToString()));
                var friends = JsonConvert.DeserializeObject<List<User>>(jsonFriends);

                if (friends.Find(u => u.UserId == user.UserId) != null) continue;

                var jsonRequested = await client.GetStringAsync(new Uri("http://localhost:2110/api/Users/RequestedUsersOfWishlist/" + wishList.WishListId.ToString()));
                var requestedUsers = JsonConvert.DeserializeObject<List<User>>(jsonRequested);
                if (requestedUsers.Find(u => u.UserId == user.UserId) != null) continue;

                wishlistsToRequest.Add(wishList);
            }

            WishlistsFriends.ItemsSource = wishlistsToRequest;

            if (wishlistsToRequest.Count != 0)
            {
                NoWishlistsFriendsOverview.Visibility = Visibility.Collapsed;
                WishlistsFriendsOverview.Visibility = Visibility.Visible;
                if (wishlistsToRequest.Count == 1)
                {
                    Button_RequestAll.Visibility = Visibility.Collapsed;
                }
                else
                {
                    Button_RequestAll.Visibility = Visibility.Visible;
                }
            }
            else
            {
                NoWishlistsFriendsOverview.Visibility = Visibility.Visible;
                WishlistsFriendsOverview.Visibility = Visibility.Collapsed;
            }
        }

        private async void Wishlists_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = ((ListView)sender).SelectedIndex;
            if (index != -1)
            {
                WishList request = wishlistsToRequest[index];

                //Process request in backend
                HttpClient client = new HttpClient();
                //TODO
                var uri = "http://localhost:2110/api/WishLists/CreateRequest/" + request.WishListId.ToString();
                var content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
                var res = await client.PostAsync(new Uri(uri), content);

                //Update list of requests and show message
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    await GetWishlistsOfSelectedFriend();
                    ContentDialog succes = new ContentDialog()
                    {
                        Title = "Request send",
                        Content = "The request to collaborate to this wishlist has been sent.",
                        CloseButtonText = "OK"
                    };
                    await succes.ShowAsync();
                }
                else
                {
                    ContentDialog fail = new ContentDialog()
                    {
                        Title = "Sending request failed",
                        Content = "Something went wrong. Please try again.",
                        CloseButtonText = "OK"
                    };
                    await fail.ShowAsync();
                }
                await GetWishlistsOfSelectedFriend();
            }
        }

        private async void RequestAll_Click(object sender, RoutedEventArgs e)
        {
            int friendid = (int)localSettings.Values[Settings.SELECTED];
            //Process request in backend
            HttpClient client = new HttpClient();
            //TODO
            var uri = "http://localhost:2110/api/Users/CreateRequestsToAllWishlistsOfUser/" + friendid.ToString();
            var content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
            var res = await client.PostAsync(new Uri(uri), content);

            //Update list of requests and show message
            if (res.StatusCode == HttpStatusCode.OK)
            {
                await GetWishlistsOfSelectedFriend();
                ContentDialog succes = new ContentDialog()
                {
                    Title = "Request send",
                    Content = "The request to collaborate to these wishlists has been send.",
                    CloseButtonText = "OK"
                };
                await succes.ShowAsync();
            }
            else
            {
                ContentDialog fail = new ContentDialog()
                {
                    Title = "Sending request failed",
                    Content = "Something went wrong. Please try again.",
                    CloseButtonText = "OK"
                };
                await fail.ShowAsync();
            }
            await GetWishlistsOfSelectedFriend();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            SystemNavigationManager.GetForCurrentView().BackRequested -= BackRequested;
        }

        private void BackRequested(object sender, BackRequestedEventArgs e)
        {
            OnBackRequested(sender, e);
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            if (Frame.CanGoBack)
            {
                e.Handled = true;
                Frame.GoBack();
            }
        }
    }
}
