﻿using Newtonsoft.Json;
using Semestertaak_Whishlist.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Semestertaak_Whishlist
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class InviteFriends : Page
    {
        private User user;
        private WishList wishList;
        private List<User> PeopleToInvite;

        public AppViewBackButtonVisibility BackButtonVisibility
        {
            get { return SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility; }
            set { SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = value; }
        }
        public InviteFriends()
        {
            InitializeComponent();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            BackButtonVisibility = Frame.CanGoBack ? AppViewBackButtonVisibility.Visible : AppViewBackButtonVisibility.Collapsed;
            SystemNavigationManager.GetForCurrentView().BackRequested += BackRequested;
            var param = e.Parameter as Object[];
            user = param[0] as User;
            wishList = param[1] as WishList;

            HttpClient client = new HttpClient();
            var json = await client.GetStringAsync(new Uri("http://localhost:2110/api/Users/"));
            var usersList = JsonConvert.DeserializeObject<List<User>>(json);

            usersList = usersList.Where(userOfList => userOfList.UserId != user.UserId).ToList();

            foreach (var friend in wishList.Friends)
            {
                usersList = usersList.Where(userOfList => userOfList.UserId != friend.UserId).ToList();
            }

            foreach (var invitedFriend in wishList.Invites)
            {
                usersList = usersList.Where(userOfList => userOfList.UserId != invitedFriend.UserId).ToList();
            }

            PeopleToInvite = usersList;
            invitedFriends.ItemsSource = PeopleToInvite;
        }

        private void txtFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            String filter = txtFilter.Text;
            List<User> filteredList = PeopleToInvite.FindAll(u => u.Username.IndexOf(filter, StringComparison.OrdinalIgnoreCase) >= 0);
            invitedFriends.ItemsSource = filteredList;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            var selectedFriends = invitedFriends.SelectedItems.ToArray();
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            int countOk = 0;
            foreach (var friendobj in selectedFriends)
            {
                User friend = friendobj as User;
                
                var uri = "http://localhost:2110/api/WishLists/inviteFriend/" + wishList.WishListId.ToString();
                var content = new StringContent(JsonConvert.SerializeObject(friend.UserId), Encoding.UTF8, "application/json");
                var res = await client.PostAsync(new Uri(uri), content);
                if (res.StatusCode == HttpStatusCode.OK) countOk++;

            }

            if (countOk == selectedFriends.Length)
            {
                Frame.Navigate(typeof(ListPage), user);
                ContentDialog succes = new ContentDialog()
                {
                    Title = "Invite send",
                    Content = "An invite has been send to all selected friends",
                    CloseButtonText = "OK"
                };
                await succes.ShowAsync();
            }
            else
            {
                ContentDialog fail = new ContentDialog()
                {
                    Title = "Invite failed",
                    Content = "Something went wrong. Please try again.",
                    CloseButtonText = "OK"
                };
                await fail.ShowAsync();
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            SystemNavigationManager.GetForCurrentView().BackRequested -= BackRequested;
        }

        private void BackRequested(object sender, BackRequestedEventArgs e)
        {
            OnBackRequested(sender, e);
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            if (Frame.CanGoBack)
            {
                e.Handled = true;
                Frame.GoBack();
            }
        }
    }
}
