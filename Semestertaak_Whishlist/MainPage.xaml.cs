﻿using Newtonsoft.Json;
using Semestertaak_Whishlist.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using Windows.Foundation;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Semestertaak_Whishlist
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
        public MainPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            ValidateJWT();
        }

        private async void ValidateJWT()
        {
            try
            {
                string token = localSettings.Values[Settings.JWT].ToString();
                ProgressOverlay.Visibility = Visibility.Visible;
                ProgressOverlay.Background.Opacity = 1;
                LoginProgress.IsActive = true;
                HttpClient client = new HttpClient();
                Uri url = new Uri("http://localhost:2110/api/Users/jwt");
                var content = new StringContent(JsonConvert.SerializeObject(token), Encoding.UTF8, "application/json");
                var result = await client.PostAsync(url, content);
                if (result.StatusCode == HttpStatusCode.NotFound)
                {
                    ProgressOverlay.Visibility = Visibility.Collapsed;
                    ProgressOverlay.Background.Opacity = 0.7;
                    LoginProgress.IsActive = false;
                    localSettings.Values.Remove(Settings.JWT);
                    ContentDialog sessionExpiredDialog = new ContentDialog
                    {
                        Title = "Session expired.",
                        Content = "Your session has expired. Please sign in again.",
                        CloseButtonText = "Ok"
                    };
                    await sessionExpiredDialog.ShowAsync();
                }
                else Frame.Navigate(typeof(ListPage), JsonConvert.DeserializeObject<User>(await result.Content.ReadAsStringAsync()));
            }
            catch (NullReferenceException)
            {
                ProgressOverlay.Visibility = Visibility.Collapsed;
                ProgressOverlay.Background.Opacity = 0.7;
                LoginProgress.IsActive = false;
            }

        }
        private async void Login_Click(object sender, RoutedEventArgs e)
        {
            ProgressOverlay.Visibility = Visibility.Visible;
            LoginProgress.IsActive = true;
            LoginButton.IsEnabled = false;
            HttpClient client = new HttpClient();
            string mail = EmailBox.Text;
            string passwd = PasswordBox.Password;
            var url = new Uri("http://localhost:2110/api/login");
            var content = new StringContent(JsonConvert.SerializeObject((mail, passwd)).ToString(), Encoding.UTF8, "application/json");
            var res = await client.PostAsync(url, content);
            if (res.StatusCode == HttpStatusCode.NotFound || res.StatusCode == HttpStatusCode.InternalServerError)
            {
                await ShowIncorrectCredentialsDialog();
                PasswordBox.Password = string.Empty;
                ProgressOverlay.Visibility = Visibility.Collapsed;
                LoginProgress.IsActive = false;
            }
            else
            {
                (User first, string second) = JsonConvert.DeserializeObject<Tuple<User, string>>(await res.Content.ReadAsStringAsync());
                localSettings.Values[Settings.JWT] = second;
                Frame.Navigate(typeof(ListPage), first);
            }
        }

        private IAsyncOperation<ContentDialogResult> ShowIncorrectCredentialsDialog()
        {
            ContentDialog incorrectCredentialsDialog = new ContentDialog
            {
                Title = "Login failed",
                Content = "Username/password incorrect. Please check if you typed them correctly.",
                CloseButtonText = "Ok"
            };
            return incorrectCredentialsDialog.ShowAsync();
        }

        private void CredentialBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter && LoginButton.IsEnabled)
            {
                Login_Click(sender, e);
            }
            LoginButton.IsEnabled = EmailBox.Text.Length > 3 && PasswordBox.Password.Length > 3;
        }

        private void Register_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Register));
        }

        private void MainCanvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            MainGrid.Width = MainCanvas.ActualWidth;
            MainGrid.Height = MainCanvas.ActualHeight;
            ProgressOverlay.Width = MainCanvas.ActualWidth;
            ProgressOverlay.Height = MainCanvas.ActualHeight;
        }
    }
}
