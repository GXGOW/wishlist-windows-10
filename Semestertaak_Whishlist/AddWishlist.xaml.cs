﻿using Semestertaak_Whishlist.Models;
using System;
using Windows.UI.Xaml.Controls;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Semestertaak_Whishlist
{
    public sealed partial class AddWishlist : ContentDialog
    {
        public String Result { get; set; }
        public WishList newList { get; set; }
        public AddWishlist()
        {
            this.InitializeComponent();
            newList = new WishList();
            mainGrid.DataContext = newList;
            this.Closing += Close;
        }

        private void Close(ContentDialog sender, ContentDialogClosingEventArgs args)
        {
            if(this.Result == "Failed")
            {
                args.Cancel = true;
            }
        }

        private void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (newList.Deadline < DateTime.Now.AddHours(5))
            {
                error.Text = "The deadline has to be after today";
                this.Result = "Failed";
            }
            else 
            if (newList.Name == null)
            {
                error.Text = "Name has to be filled in";
                this.Result = "Failed";
            }
            else
            {
                this.Result = "Success";
            }
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            this.Result = "Cancelled";
        }


    }
}
