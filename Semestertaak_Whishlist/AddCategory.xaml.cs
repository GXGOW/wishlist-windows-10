﻿using Semestertaak_Whishlist.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Semestertaak_Whishlist
{
    public sealed partial class AddCategory : ContentDialog
    {
        public Category Category { get; set; }
        public String Result { get; set; }
        public AddCategory()
        {
            this.InitializeComponent();
            Category = new Category();
            mainGrid.DataContext = Category;
            this.Closing += Close;
        }

        private void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (Category.Name == null)
            {
                error.Text = "Name has to be filled in";
                this.Result = "Failed";
            }
            else
            {
                this.Result = "Success";
            }
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            this.Result = "Cancelled";
        }

        private void Close(ContentDialog sender, ContentDialogClosingEventArgs args)
        {
            if (this.Result == "Failed")
            {
                args.Cancel = true;
            }
        }
    }
}
