﻿using Newtonsoft.Json;
using Semestertaak_Whishlist.Models;
using Semestertaak_Whishlist.ViewModels;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Semestertaak_Whishlist
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class UserSetting : Page
    {
        private User user;
        private UserSettingPasswordViewModel userSettingPasswordViewModel;
        private UserSettingEmailViewModel userSettingEmailViewModel;
        private UserSettingProfilePictureViewModel userSettingProfilePictureViewModel;

        public AppViewBackButtonVisibility BackButtonVisibility
        {
            get { return SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility; }
            set { SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = value; }
        }
        public UserSetting()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            BackButtonVisibility = Frame.CanGoBack ? AppViewBackButtonVisibility.Visible : AppViewBackButtonVisibility.Collapsed;
            SystemNavigationManager.GetForCurrentView().BackRequested += BackRequested;
            var titleBar = ApplicationView.GetForCurrentView().TitleBar;
            user = e.Parameter as User;
            userSettingPasswordViewModel = new UserSettingPasswordViewModel();
            userSettingEmailViewModel = new UserSettingEmailViewModel();
            Email.DataContext = userSettingEmailViewModel;
            Password.DataContext = userSettingPasswordViewModel;
            userSettingProfilePictureViewModel = new UserSettingProfilePictureViewModel();
            ProfilePic.DataContext = userSettingProfilePictureViewModel;
            userSettingProfilePictureViewModel.image = user.ProfilePicture;
        }

        private async void ChangeMail(object sender, RoutedEventArgs e)
        {
            ErrorMail.Text = "";
            ErrorMailRepeat.Text = "";
            if (userSettingEmailViewModel.NewEmail == null || !Regex.IsMatch(userSettingEmailViewModel.NewEmail,
                @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                RegexOptions.IgnoreCase))
            {
                ErrorMail.Text = "Email is not valid";
            }
            else if (userSettingEmailViewModel.NewEmail != userSettingEmailViewModel.RepeatEmail)
            {
                ErrorMailRepeat.Text = "New email and repeat email don't match";
            }
            else
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var uri = "http://localhost:2110/api/Users/UpdateEmail/" + user.UserId.ToString();
                var content = new StringContent(JsonConvert.SerializeObject(userSettingEmailViewModel), Encoding.UTF8, "application/json");
                await client.PutAsync(new Uri(uri), content);
                ContentDialog success = new ContentDialog()
                {
                    Title = "Email updated",
                    Content = "Your email has successfully been updated. This new email needs to be used to log in now.",
                    CloseButtonText = "OK"
                };
                await success.ShowAsync();
                user.Username = userSettingEmailViewModel.NewEmail;
            }
        }

        private async void ChangePassword(object sender, RoutedEventArgs e)
        {
            ErrorPassword.Text = "";
            ErrorPasswordRepeat.Text = "";
            if (userSettingPasswordViewModel.NewPassword == null || userSettingPasswordViewModel.NewPassword == "" || userSettingPasswordViewModel.NewPassword.Length < 8)
            {
                ErrorPassword.Text = "Password has to be at least 8 characters";
            }
            else if (userSettingPasswordViewModel.NewPassword != userSettingPasswordViewModel.RepeatPassword)
            {
                ErrorPasswordRepeat.Text = "New password and repeat password don't match";
            }
            else
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var uri = "http://localhost:2110/api/Users/UpdatePassword/" + user.UserId.ToString();
                var content = new StringContent(JsonConvert.SerializeObject(userSettingPasswordViewModel), Encoding.UTF8, "application/json");
                await client.PutAsync(new Uri(uri), content);
                ContentDialog success = new ContentDialog()
                {
                    Title = "Password updated",
                    Content = "Your password has successfully been updated. This new password needs to be used to log in now.",
                    CloseButtonText = "OK"
                };
                await success.ShowAsync();
            }
        }

        private void BackRequested(object sender, BackRequestedEventArgs e)
        {
            OnBackRequested(sender, e);
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            if (Frame.CanGoBack)
            {
                e.Handled = true;
                Frame.GoBack();
            }
        }

        private async void ChooseImage(object sender, RoutedEventArgs e)
        {
            FileOpenPicker picker = new FileOpenPicker();
            picker.FileTypeFilter.Add(".bmp");
            picker.FileTypeFilter.Add(".png");
            picker.FileTypeFilter.Add(".jpg");
            StorageFile file = await picker.PickSingleFileAsync();
            if (file != null)
            {
                using (var inputStream = await file.OpenSequentialReadAsync())
                {
                    var readStream = inputStream.AsStreamForRead();
                    byte[] buffer = new byte[readStream.Length];
                    await readStream.ReadAsync(buffer, 0, buffer.Length);
                    userSettingProfilePictureViewModel.image = buffer;
                }
            }
        }

        private async void UploadImage(object sender, RoutedEventArgs e)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var uri = "http://localhost:2110/api/Users/UpdateProfilePicture/" + user.UserId.ToString();
            var content = new ByteArrayContent(userSettingProfilePictureViewModel.image);
            await client.PutAsync(new Uri(uri), content);
            ContentDialog success = new ContentDialog()
            {
                Title = "Profile picture updated",
                Content = "Your profile picture has successfully been updated.",
                CloseButtonText = "OK"
            };
            await success.ShowAsync();
            user.ProfilePicture = userSettingProfilePictureViewModel.image;
        }
    }
}
